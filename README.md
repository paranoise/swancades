# SwanCades #

SwanCades is mobile application intended to quickly provide information about arcades and arcade games in the city of Perth.

SwanCades is built using HaxeUI, for the OpenFL backend.

### Roadmap/ Planned Feature List ###

- [x] Basic display of both arcade locations and games at each location.
- [ ] Arcade machine status.
- [ ] User submissions for new entries for both arcade machines and arcades.
- [ ] Display closest arcade via GPS.
- [x] Arcade machine search.
- [ ] Machine filtering by rating.
- [ ] Proximity machine search.
- [ ] A usable UI (stretch goal).

### Dependencies ###

All dependencies can be installed with Haxelib. The full list of dependencies is below:

* [OpenFL](https://lib.haxe.org/p/openfl/)
* [HaxeUI Core](https://lib.haxe.org/p/haxeui-core/)
* [HaxeUI OpenFL Backend](https://lib.haxe.org/p/haxeui-openfl/)

### Building ###

SwanCades is designed and tested to run on Android. Support for iOS is not planned as Lime iOS builds require Xcode and I have no access to MacOS or an iOS device for testing. It should be relatively easy to port SwanCades to iOS.

SwanCades can be built with `openfl build android`.

### Acknowledgements ###

- A lot of the data provided in the arcades.json file comes courtesy of the [WA Metro Arcade Guide](https://docs.google.com/spreadsheets/d/1F876Om6PQs_bTiDmT36SuNYMrqYCcH_LyBzNzLW3tDk/edit#gid=1449331314). If contributing changes to the arcades.json file, please also submit those changes to the Metro Arcade Guide via the Google form available on the spreadsheet.