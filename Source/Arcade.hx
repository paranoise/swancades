class Arcade{
	public var brand:String;
	public var name:String;
	public var location:String;
	public var hours:Array<String>;

	public var games:Array<Game> = [];

	public function new(_arc:Dynamic){
		brand = _arc.brand;
		name = _arc.name;
		location = _arc.location;
		hours = _arc.hours;

		var _games:Array<Dynamic> = Reflect.copy(_arc.games);
		
		for(g in _games){
			var gToAdd = new Game(g, this);
			games.push(gToAdd);
		}
	}
}