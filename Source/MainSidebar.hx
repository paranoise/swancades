import haxe.ui.events.UIEvent;

@:build(haxe.ui.macros.ComponentMacros.build("Assets/sidebar.xml"))
class MainSidebar extends haxe.ui.containers.SideBar {
	public function new(){
		super();

		jsonloc.text = Main.getJson();
	}

	@:bind(jsonloc, UIEvent.CHANGE)
	function onLocUpdate(e){
		Main.swapJson(jsonloc.text);
	}
}
