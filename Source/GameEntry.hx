package ;

import haxe.ui.containers.HBox;
import haxe.ui.containers.dialogs.Dialog;
import haxe.ui.events.MouseEvent;

@:build(haxe.ui.ComponentBuilder.build("Assets/game.xml"))
class GameEntry extends HBox {
	public var gname:String;
	var price:String;
	var quality:String;
	var notes:String;
	var gimg:String;

	var aname:String;
	var location:String;
	
	public function new(game:Game) {
		super();

		gname = game.name;
		price = game.price;
		notes = game.notes;
		aname = game.arcade.name;
		location = game.arcade.location;
		quality = game.quality;
		gimg = game.img;

		gamename.text = gname;
		gameprice.text = price;
		gamequality.text = quality;
		
	}

	@:bind(gamenotes, MouseEvent.CLICK)
	private function onCustomModalDialog(e) {
		var dialog = new GameInfoDialog(gname, notes, price, quality, aname, location, gimg);
		dialog.showDialog();
	}
}

@:build(haxe.ui.macros.ComponentMacros.build("Assets/game-dialog.xml"))
class GameInfoDialog extends Dialog {
	public function new(gameName:String, notes:String, price:String, quality:String, arcadeName:String, location:String, img:String) {
		super();
		title = gameName;
		
		gname.text = gameName;
		gprice.text = price + " per Credit";
		gquality.text = "Rating: " + quality;
		gloc.text = "At: " + arcadeName + ", " + location;

		if(img != "none"){
			machinepic.resource = img;
		}
		
		gnotes.text = notes;
		buttons = "Close";
	}
}